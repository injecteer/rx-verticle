package io.playground;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import io.vertx.core.json.JsonObject;
import io.vertx.rxjava.core.AbstractVerticle;
import io.vertx.rxjava.core.eventbus.Message;
import io.vertx.rxjava.ext.web.Router;
import io.vertx.rxjava.ext.web.handler.BodyHandler;
import rx.Observable;

public class RxVerticle extends AbstractVerticle {

  private final Logger log = LoggerFactory.getLogger( RxVerticle.class );
  
  private Map<String, String> mapping = new HashMap<>();

  public RxVerticle() {
    mapping.put( "sum", "summarizer" );
    mapping.put( "mul", "multiplier" );
    mapping.put( "str", "stringifier" );
    mapping.put( "pow", "power2" );
  }
  
  @Override
  public void start() throws Exception {
    Router router = Router.router( vertx );
    router.route().handler( BodyHandler.create() );
    router.route( "/" ).handler( rc -> rc.response().end( "RX ok" ) );

    router.postWithRegex( "/\\w+" ).produces( "application/json" ).handler( rc -> {
      String path = rc.request().path();
      JsonObject body = rc.getBodyAsJson();
      log.info( "<< " + path + ": " + body );
      
      
      Optional<Observable<JsonObject>> opt = mapping.entrySet().stream()
        .filter( e -> path.contains( e.getKey() ) )
        .map( e -> vertx.eventBus().<JsonObject>rxSend( e.getValue(), body ).map( Message::body ).toObservable() )
        .reduce( ( acc, cur ) -> acc.mergeWith( cur ) );
      
      if( opt.isPresent() ){
        JsonObject res = new JsonObject();
        opt.get().subscribe(
          o -> {
            log.info( "~~ o = " + o );
            o.forEach( e -> res.put( e.getKey(), e.getValue() ) );
          }, 
          t -> rc.response().setStatusCode( 400 ).end( "{\"message\":\"" + t.getMessage() + "\"}" ), 
          () -> {
            log.info( ">> res = " + res );
            rc.response().end( res.encode() );
          } 
        );
      }else
        rc.response().setStatusCode( 204 ).end(); 
    } );

    vertx.createHttpServer().requestHandler( router ).listen( 8091 );
  }

}
